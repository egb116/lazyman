
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FindURL {

    private final String date;
    private List<ArrayList<String>> gameinfo, availableStreams = new ArrayList(15);;
    private final int gameType;
    private int foundGames = -1;
    private Timezone tz;

    public FindURL(String d, int gt) {
        date = d;
        gameinfo = new ArrayList(15);
        tz = new Timezone();
        gameType = gt;
        foundGames = search();
    }

    public String getText(String url) {
        Reader r = null;

        URLConnection con;
        URL website;
        try {
            try {
                website = new URL(url);
            } catch (MalformedURLException j) {
                return "down";
            }
            con = website.openConnection();
            InputStream is = con.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            char[] charArray = new char['?'];
            StringBuilder sb = new StringBuilder();
            int numCharsRead;
            while ((numCharsRead = isr.read(charArray)) > 0) {
                sb.append(charArray, 0, numCharsRead);
            }
            return sb.toString();
        } catch (IOException ex) {
            System.err.println(ex);
            return "";
        } catch (NullPointerException n) {
            n.printStackTrace();
            return "offline";
        } finally {
            try {
                if (r != null) {
                    r.close();
                    r = null;
                }
            } catch (IOException localIOException5) {
            }
        }
    }

    private int search() {
        String jsonText = getText("http://statsapi.web.nhl.com/api/v1/schedule?startDate=" + date + "&endDate=" + date + "&expand=schedule.teams,schedule.game.content.media.epg");
        if (jsonText.equals("offline")) {
            return -2;
        }
        int l = gotLive(jsonText);
        jsonText = null;
        System.gc();
        return l;
    }

    public ArrayList getTdyGms() {
        ArrayList j = new ArrayList();
        for (int i = 0; i < gameinfo.size(); i++) {
            if (((ArrayList) gameinfo.get(i)).get(0) != null) {
                String gameInfo;
                if (gameType == 0) {
                    gameInfo = String.format("%-3s%5s%8s%10s", new Object[]{((ArrayList) gameinfo.get(i)).get(2), "@", ((ArrayList) gameinfo.get(i)).get(3), tz.toLocalTZ(((String) ((ArrayList) gameinfo.get(i)).get(1)).toString()).substring(10)});
                } else {
                    gameInfo = String.format("%-3s%5s%8s%10s", new Object[]{((ArrayList) gameinfo.get(i)).get(1), "@", ((ArrayList) gameinfo.get(i)).get(2), "FINAL"});
                }
                j.add(gameInfo);
            }
        }
        return j;
    }

    public boolean testM3U8(String url) {
        try {
            URL u = new URL(url);
            HttpURLConnection huc = (HttpURLConnection) u.openConnection();
            huc.setRequestMethod("HEAD");
            huc.connect();
            return huc.getResponseCode() == 200;
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            return false;
        } catch (IOException ex) {
            System.err.println("v");
        }
        return false;
    }

    public String getGameID(int id) {
        try {
            return ((String) ((ArrayList) gameinfo.get(id)).get(0)).toString();
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }

    public String GetHomeTeam(int id) {
        return ((String) ((ArrayList) gameinfo.get(id)).get(3)).toString();
    }

    public boolean isAvailable(String streamType, int id) {
        if (availableStreams.get(id).contains("ALL"))
            return true;
        
        return availableStreams.get(id).contains(streamType);
    }

    public String GetAwayTeam(int id) {
        return ((String) ((ArrayList) gameinfo.get(id)).get(2)).toString();
    }

    public String GetGameState(int id) {
        return ((String) ((ArrayList) gameinfo.get(id)).get(4)).toString();
    }

    public String GetDate(int id) {
        return ((String) ((ArrayList) gameinfo.get(id)).get(1)).toString().substring(0, 10);
    }

    public String GetTime(int id) {
        return ((String) ((ArrayList) gameinfo.get(id)).get(1)).toString().substring(10);
    }

    private void sort(List<ArrayList<String>> gameinfo) {
        boolean flag = true;
        while (flag) {
            flag = false;
            for (int j = 0; j < gameinfo.size() - 1; j++) {
                if (j + 1 == gameinfo.size()) {
                    break;
                }
                String t1 = ((String) ((ArrayList) gameinfo.get(j)).get(1)).toString();
                String t2 = ((String) ((ArrayList) gameinfo.get(j + 1)).get(1)).toString();
                if (tz.compareTime(t1, t2)) {
                    Collections.swap(gameinfo, j, j + 1);
                    flag = true;
                }
            }
        }
    }

    

    private String getSeason() {
        String year;
        String month;
        if (!date.contains("/")) {
            month = date.substring(4, 6);
            year = date.substring(0, 4);
        } else {
            year = date.substring(6, date.length());
            month = date.substring(0, 2);
        }
        int y = Integer.parseInt(year);
        int m = Integer.parseInt(month);
        if ((m >= 1) && (m <= 8)) {
            y--;
        }
        m = y++;
        return "" + m + y;
    }

    private int gotLive(String jsonText) {
        if ((!jsonText.equals("")) && (!jsonText.equals("offline"))) {
            try {
                JsonParser parser = new JsonParser();
                Object o = parser.parse(jsonText);
                parser = null;
                jsonText = null;
                System.gc();

                JsonObject t = (JsonObject) o;
                JsonArray games = t.getAsJsonArray("dates");
                o = null;

                int i = 0;
                ArrayList row = null, row2 = null;

                t = (JsonObject) games.get(0);
                games = t.getAsJsonArray("games");
                while (i < games.size()) {
                    row = new ArrayList();
                    row2 = new ArrayList();
                    t = (JsonObject) games.get(i);
                    row.add(t.get("gamePk").getAsString());
                    row.add(t.get("gameDate").getAsString().replace("T", " ").replace("Z", ""));
                    JsonObject tmpObj = t;

                    JsonObject t2 = (JsonObject) t.get("teams");
                    t2 = (JsonObject) t2.get("away");
                    t2 = (JsonObject) t2.get("team");
                    row.add(t2.get("abbreviation").getAsString());
                    t = tmpObj;

                    t2 = (JsonObject) t.get("teams");
                    t2 = (JsonObject) t2.get("home");
                    t2 = (JsonObject) t2.get("team");
                    row.add(t2.get("abbreviation").getAsString());
                    t = tmpObj;

                    t = t.getAsJsonObject("status");
                    row.add(t.get("detailedState").getAsString());
                    try {
                        t = tmpObj;
                        t = t.getAsJsonObject("content");
                        t = (JsonObject) t.get("media");
                        JsonArray a = t.getAsJsonArray("epg");
                        t = (JsonObject) a.get(0);
                        a = t.getAsJsonArray("items");

                        for (int j = 0; j < a.size() - 1; j++) {
                            t = (JsonObject) a.get(j);
                            row2.add(t.get("mediaFeedType").getAsString());
                        }
                    } catch (NullPointerException npe) {
                        npe.printStackTrace();
                        row2.add("ALL");
                    }

                    gameinfo.add(row);
                    availableStreams.add(row2);

                    i++;
                }
                games = null;
                if (gameinfo.isEmpty()) {
                    return 0;
                }
                sort(gameinfo);
                return 1;
            } catch (IndexOutOfBoundsException iob) {
                return 0;
            } catch (JsonSyntaxException ex) {
                System.err.println(ex);
            }
        }
        if (jsonText.equals("offline")) {
            return -2;
        }
        return 2;
    }

    public int GamesFound() {
        return foundGames;
    }

    public String getFavTeamIndex(String team) {
        for (int i = 0; i < gameinfo.size(); i++) {
            if (((String) ((ArrayList) gameinfo.get(i)).get(2)).equals(team)) {
                return i + "a";
            }
            if (((String) ((ArrayList) gameinfo.get(i)).get(3)).equals(team)) {
                return i + "h";
            }
        }
        return "-1n";
    }
}
