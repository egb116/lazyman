/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.OutputStream;
/*     */ import java.nio.file.Path;
/*     */ import java.nio.file.Paths;
/*     */ import java.util.Properties;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Prop
/*     */ {
/*     */   private final Properties prop;
/*     */   private String c;
/*     */   
/*     */   public Prop(String os)
/*     */     throws IOException
/*     */   {
/*  27 */     this.prop = new Properties();
/*  28 */     char slash = '/';
/*  29 */     if (os.contains("win")) {
/*  30 */       slash = '\\';
/*     */     }
/*  32 */     Path currentRelativePath = Paths.get("", new String[0]);
/*  33 */     this.c = (currentRelativePath.toAbsolutePath().toString() + slash + "config.properties");
/*     */   }
/*     */   
/*     */ 
/*     */   public String getVlcloc()
/*     */   {
/*     */     try
/*     */     {
/*  41 */       InputStream input = null;
/*  42 */       input = new FileInputStream(this.c);
/*     */       
/*     */ 
/*  45 */       this.prop.load(input);
/*  46 */       input.close();
/*  47 */       if (this.prop.getProperty("VLCLocation") != null) {
/*  48 */         return this.prop.getProperty("VLCLocation");
/*     */       }
/*  50 */       return "";
/*     */     }
/*     */     catch (IOException e) {}
/*  53 */     return "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setVlcloc(String vlcloc)
/*     */   {
/*  61 */     OutputStream output = null;
/*     */     try {
/*  63 */       output = new FileOutputStream(this.c);
/*     */       
/*     */ 
/*  66 */       this.prop.setProperty("VLCLocation", vlcloc);
/*  67 */       this.prop.store(output, ""); return;
/*     */     }
/*     */     catch (IOException e) {
/*  70 */       e.printStackTrace();
/*     */     } finally {
/*  72 */       if (output != null) {
/*     */         try {
/*  74 */           output.close();
/*     */         } catch (IOException e) {
/*  76 */           e.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String getJarloc()
/*     */   {
/*     */     try
/*     */     {
/*  88 */       InputStream input = null;
/*  89 */       input = new FileInputStream(this.c);
/*     */       
/*     */ 
/*  92 */       this.prop.load(input);
/*  93 */       input.close();
/*  94 */       if (this.prop.getProperty("JARLocation") != null) {
/*  95 */         return this.prop.getProperty("JARLocation");
/*     */       }
/*  97 */       return "";
/*     */     }
/*     */     catch (IOException e) {}
/* 100 */     return "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setJarloc(String jarloc)
/*     */   {
/* 108 */     OutputStream output = null;
/*     */     try {
/* 110 */       output = new FileOutputStream(this.c);
/*     */       
/*     */ 
/* 113 */       this.prop.setProperty("JARLocation", jarloc);
/* 114 */       this.prop.store(output, ""); return;
/*     */     } catch (IOException e) {
/* 116 */       e.printStackTrace();
/*     */     } finally {
/* 118 */       if (output != null) {
/*     */         try {
/* 120 */           output.close();
/*     */         } catch (IOException e) {
/* 122 */           e.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public String getPW()
/*     */   {
/*     */     try {
/* 131 */       InputStream input = null;
/* 132 */       input = new FileInputStream(this.c);
/*     */       
/*     */ 
/* 135 */       this.prop.load(input);
/* 136 */       input.close();
/* 137 */       if (this.prop.getProperty("Pass") != null) {
/* 138 */         return this.prop.getProperty("Pass");
/*     */       }
/* 140 */       return "";
/*     */     }
/*     */     catch (IOException e) {}
/* 143 */     return "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setPW(String pw)
/*     */   {
/* 151 */     OutputStream output = null;
/*     */     try {
/* 153 */       output = new FileOutputStream(this.c);
/*     */       
/*     */ 
/* 156 */       this.prop.setProperty("Pass", pw);
/* 157 */       this.prop.store(output, ""); return;
/*     */     }
/*     */     catch (IOException e) {
/* 160 */       e.printStackTrace();
/*     */     } finally {
/* 162 */       if (output != null) {
/*     */         try {
/* 164 */           output.close();
/*     */         } catch (IOException e) {
/* 166 */           e.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void setBitrate(String br)
/*     */   {
/* 174 */     OutputStream output = null;
/*     */     try {
/* 176 */       output = new FileOutputStream(this.c);
/*     */       
/* 178 */       this.prop.setProperty("Bitrate", br);
/* 179 */       this.prop.store(output, ""); return;
/*     */     }
/*     */     catch (IOException e) {
/* 182 */       e.printStackTrace();
/*     */     } finally {
/* 184 */       if (output != null) {
/*     */         try {
/* 186 */           output.close();
/*     */         } catch (IOException e) {
/* 188 */           e.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public String getBitrate()
/*     */   {
/*     */     try {
/* 197 */       InputStream input = null;
/* 198 */       input = new FileInputStream(this.c);
/*     */       
/*     */ 
/* 201 */       this.prop.load(input);
/* 202 */       input.close();
/* 203 */       if (this.prop.getProperty("Bitrate") != null) {
/* 204 */         return this.prop.getProperty("Bitrate");
/*     */       }
/* 206 */       return "";
/*     */     }
/*     */     catch (IOException e) {}
/* 209 */     return "";
/*     */   }
/*     */   
/*     */   public void setCDN(String cdn)
/*     */   {
/* 214 */     OutputStream output = null;
/*     */     try {
/* 216 */       output = new FileOutputStream(this.c);
/*     */       
/*     */ 
/* 219 */       this.prop.setProperty("CDN", cdn);
/* 220 */       this.prop.store(output, ""); return;
/*     */     }
/*     */     catch (IOException e) {
/* 223 */       e.printStackTrace();
/*     */     } finally {
/* 225 */       if (output != null) {
/*     */         try {
/* 227 */           output.close();
/*     */         } catch (IOException e) {
/* 229 */           e.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public String getCDN()
/*     */   {
/*     */     try {
/* 238 */       InputStream input = null;
/* 239 */       input = new FileInputStream(this.c);
/*     */       
/*     */ 
/* 242 */       this.prop.load(input);
/* 243 */       input.close();
/* 244 */       if (this.prop.getProperty("CDN") != null) {
/* 245 */         return this.prop.getProperty("CDN");
/*     */       }
/* 247 */       return "";
/*     */     }
/*     */     catch (IOException e) {}
/* 250 */     return "";
/*     */   }
/*     */   
/*     */   public String getNHLTeam()
/*     */   {
/*     */     try {
/* 256 */       InputStream input = null;
/* 257 */       input = new FileInputStream(this.c);
/*     */       
/*     */ 
/* 260 */       this.prop.load(input);
/* 261 */       input.close();
/* 262 */       if (this.prop.getProperty("NHLTeam") != null) {
/* 263 */         return this.prop.getProperty("NHLTeam");
/*     */       }
/* 265 */       return "";
/*     */     }
/*     */     catch (IOException e) {}
/* 268 */     return "";
/*     */   }
/*     */   
/*     */   public void setNHLTeam(String jarloc)
/*     */   {
/* 273 */     OutputStream output = null;
/*     */     try {
/* 275 */       output = new FileOutputStream(this.c);
/*     */       
/*     */ 
/* 278 */       this.prop.setProperty("NHLTeam", jarloc);
/* 279 */       this.prop.store(output, ""); return;
/*     */     } catch (IOException e) {
/* 281 */       e.printStackTrace();
/*     */     } finally {
/* 283 */       if (output != null) {
/*     */         try {
/* 285 */           output.close();
/*     */         } catch (IOException e) {
/* 287 */           e.printStackTrace();
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              D:\Program Files\NetBeansProjects\LazyMan2\LazyMan2.jar!\Prop.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */