/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import java.io.IOException;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ import javax.swing.GroupLayout;
/*     */ import javax.swing.GroupLayout.Alignment;
/*     */ import javax.swing.GroupLayout.ParallelGroup;
/*     */ import javax.swing.GroupLayout.SequentialGroup;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JComboBox;
/*     */ import javax.swing.JDialog;
/*     */ import javax.swing.JLabel;
/*     */ 
/*     */ public class Options extends JDialog
/*     */ {
/*     */   private Map<String, String> nhl;
/*  18 */   private Prop p = null;
/*     */   private JComboBox NHLCB;
/*     */   private JButton applyBtn;
/*     */   
/*     */   public Options(java.awt.Frame parent, boolean modal)
/*     */   {
/*  24 */     super(parent, modal);
/*  25 */     initComponents();
/*     */     
/*  27 */     this.nhl = new java.util.TreeMap();
/*  28 */     setNHLMap();
/*     */     try
/*     */     {
/*  31 */       this.p = new Prop(System.getProperty("os.name").toLowerCase());
/*     */     } catch (IOException ex) {
/*  33 */       ex.printStackTrace();
/*     */     }
/*     */     
/*  36 */     if (this.p != null)
/*     */     {
/*     */ 
/*  39 */       String team = this.p.getNHLTeam();
/*  40 */       if (!team.equals("")) {
/*  41 */         for (Map.Entry<String, String> e : this.nhl.entrySet()) {
/*  42 */           if (team.equals(e.getValue())) {
/*  43 */             team = (String)e.getKey();
/*  44 */             break;
/*     */           }
/*     */         }
/*  47 */         this.NHLCB.setSelectedItem(team);
/*  48 */         if (!this.p.getNHLTeam().equals("")) {
/*  49 */           this.applyBtn.setEnabled(false);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void initComponents()
/*     */   {
/*  64 */     this.jLabel1 = new JLabel();
/*  65 */     this.applyBtn = new JButton();
/*  66 */     this.closeBtn = new JButton();
/*  67 */     this.jLabel4 = new JLabel();
/*  68 */     this.NHLCB = new JComboBox();
/*     */     
/*  70 */     setDefaultCloseOperation(2);
/*  71 */     setTitle("Options");
/*     */     
/*  73 */     this.jLabel1.setText("Select your favorite team:");
/*     */     
/*  75 */     this.applyBtn.setText("Apply");
/*  76 */     this.applyBtn.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  78 */         Options.this.applyBtnActionPerformed(evt);
/*     */       }
/*     */       
/*  81 */     });
/*  82 */     this.closeBtn.setText("Close");
/*  83 */     this.closeBtn.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  85 */         Options.this.closeBtnActionPerformed(evt);
/*     */       }
/*     */       
/*  88 */     });
/*  89 */     this.jLabel4.setText("NHL:");
/*     */     
/*  91 */     this.NHLCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Anaheim Ducks", "Arizona Coyotes", "Boston Bruins", "Buffalo Sabres", "Calgary Flames", "Carolina Hurricanes", "Chicago Blackhawks", "Colorado Avalanche", "Columbus Blue Jackets", "Dallas Stars", "Detroit Red Wings", "Edmonton Oilers", "Florida Panthers", "Los Angeles Kings", "Minnesota Wild", "Montreal Canadiens", "Nashville Predators", "New Jersey Devils", "New York Islanders", "New York Rangers", "Ottawa Senators", "Philadelphia Flyers", "Pittsburgh Penguins", "San Jose Sharks", "St. Louis Blues", "Tampa Bay Lightning", "Toronto Maple Leafs", "Vancouver Canucks", "Washington Capitals", "Winnipeg Jets" }));
/*  92 */     this.NHLCB.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent evt) {
/*  94 */         Options.this.NHLCBActionPerformed(evt);
/*     */       }
/*     */       
/*  97 */     });
/*  98 */     GroupLayout layout = new GroupLayout(getContentPane());
/*  99 */     getContentPane().setLayout(layout);
/* 100 */     layout.setHorizontalGroup(layout
/* 101 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 102 */       .addGroup(layout.createSequentialGroup()
/* 103 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/* 104 */       .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
/* 105 */       .addGap(0, 0, 32767)
/* 106 */       .addComponent(this.applyBtn)
/* 107 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 108 */       .addComponent(this.closeBtn))
/* 109 */       .addGroup(layout.createSequentialGroup()
/* 110 */       .addContainerGap()
/* 111 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
/* 112 */       .addComponent(this.jLabel1)
/* 113 */       .addGroup(layout.createSequentialGroup()
/* 114 */       .addComponent(this.jLabel4)
/* 115 */       .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
/* 116 */       .addComponent(this.NHLCB, -2, 143, -2)))
/* 117 */       .addGap(0, 82, 32767)))
/* 118 */       .addContainerGap()));
/*     */     
/* 120 */     layout.setVerticalGroup(layout
/* 121 */       .createParallelGroup(GroupLayout.Alignment.LEADING)
/* 122 */       .addGroup(layout.createSequentialGroup()
/* 123 */       .addComponent(this.jLabel1, -2, 14, -2)
/* 124 */       .addGap(18, 18, 18)
/* 125 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 126 */       .addComponent(this.jLabel4, -2, 19, -2)
/* 127 */       .addComponent(this.NHLCB, -2, -1, -2))
/* 128 */       .addGap(27, 27, 27)
/* 129 */       .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
/* 130 */       .addComponent(this.closeBtn)
/* 131 */       .addComponent(this.applyBtn))
/* 132 */       .addContainerGap(-1, 32767)));
/*     */     
/*     */ 
/* 135 */     pack();
/*     */   }
/*     */   
/*     */   private void applyBtnActionPerformed(ActionEvent evt) {
/* 139 */     this.p.setNHLTeam((String)this.nhl.get((String)this.NHLCB.getSelectedItem()));
/* 140 */     this.applyBtn.setEnabled(false);
/*     */   }
/*     */   
/*     */   private void closeBtnActionPerformed(ActionEvent evt)
/*     */   {
/* 145 */     dispose();
/*     */   }
/*     */   
/*     */   private void NHLCBActionPerformed(ActionEvent evt)
/*     */   {
/* 150 */     if (!this.p.getNHLTeam().equals(this.nhl.get(this.NHLCB.getSelectedItem()))) {
/* 151 */       this.applyBtn.setEnabled(true);
/*     */     } else {
/* 153 */       this.applyBtn.setEnabled(false);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   private JButton closeBtn;
/*     */   
/*     */   private JLabel jLabel1;
/*     */   
/*     */   private JLabel jLabel4;
/*     */   
/*     */   private void setNHLMap()
/*     */   {
/* 167 */     this.nhl.put("None", "None");
/* 168 */     this.nhl.put("Ottawa Senators", "OTT");
/* 169 */     this.nhl.put("Philadelphia Flyers", "PHI");
/* 170 */     this.nhl.put("New York Rangers Rangers", "NYR");
/* 171 */     this.nhl.put("Washington Capitals", "WSH");
/* 172 */     this.nhl.put("Calgary Flames", "CGY");
/* 173 */     this.nhl.put("Winnipeg Jets", "WPG");
/* 174 */     this.nhl.put("San Jose Sharks", "SJS");
/* 175 */     this.nhl.put("Los Angeles Kings", "LAK");
/* 176 */     this.nhl.put("Minnesota Wild", "MIN");
/* 177 */     this.nhl.put("St Louis Blues", "STL");
/* 178 */     this.nhl.put("Pittsburgh Penguins", "PIT");
/* 179 */     this.nhl.put("Buffalo Sabres", "BUF");
/* 180 */     this.nhl.put("Montreal Canadiens", "MTL");
/* 181 */     this.nhl.put("Toronto Maple Leafs", "TOR");
/* 182 */     this.nhl.put("New Jersey Devils", "NJD");
/* 183 */     this.nhl.put("Florida Panthers", "FLA");
/* 184 */     this.nhl.put("Columbus Blue Jackets", "CBJ");
/* 185 */     this.nhl.put("New York Islanders", "NYI");
/* 186 */     this.nhl.put("Detroit Red Wings", "DET");
/* 187 */     this.nhl.put("Carolina Hurricanes", "CAR");
/* 188 */     this.nhl.put("Boston Bruins", "BOS");
/* 189 */     this.nhl.put("Tampa Bay Lightning", "TBL");
/* 190 */     this.nhl.put("Nashville Predators", "NSH");
/* 191 */     this.nhl.put("Dallas Stars", "DAL");
/* 192 */     this.nhl.put("Chicago Blackhawks", "CHI");
/* 193 */     this.nhl.put("Colorado Avalanche", "COL");
/* 194 */     this.nhl.put("Anaheim Ducks", "ANA");
/* 195 */     this.nhl.put("Arizona Coyotes", "ARI");
/* 196 */     this.nhl.put("Edmonton Oilers", "EDM");
/* 197 */     this.nhl.put("Vancouver Canucks", "VAN");
/*     */   }
/*     */ }


/* Location:              D:\Program Files\NetBeansProjects\LazyMan2\LazyMan2.jar!\Options.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */