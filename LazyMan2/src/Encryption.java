class Encryption
{
  static String key = "99lhkj";
  
  public static String encrypt(String str)
  {
    StringBuilder sb = new StringBuilder(str);
    
    int lenStr = str.length();
    int lenKey = key.length();
    
    int i = 0;
    for (int j = 0; i < lenStr; j++)
    {
      if (j >= lenKey) {
        j = 0;
      }
      sb.setCharAt(i, (char)(str.charAt(i) ^ key.charAt(j)));i++;
    }
    return sb.toString();
  }
  
  public static String decrypt(String str)
  {
    return encrypt(str);
  }
}
