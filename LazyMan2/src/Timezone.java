
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Timezone {

    private String fmt = "";
    private String fmt2, fmt3;

    public Timezone() {
        this.fmt = "yyyy-MM-dd";
        this.fmt2 = "yyyyMMdd";
        fmt3 = "MM/dd/yyyy";
    }

    public String getPSTDate(int formatType) {
        Date today = new Date();
        DateFormat df;
        if (formatType == 0) {
            df = new SimpleDateFormat(this.fmt);
        } else if (formatType == 1) {
            df = new SimpleDateFormat(this.fmt2);
        } else {
            df = new SimpleDateFormat(this.fmt3);
        }

        df.setTimeZone(TimeZone.getDefault());
        df.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));

        return df.format(today);
    }

    public String getUTCDate(int formatType) {
        Date today = new Date();
        try {
            DateFormat df;
            if (formatType == 0) {
                df = new SimpleDateFormat("yyyy/MM/dd");
            } else {
                df = new SimpleDateFormat(this.fmt2);
            }

            df.setTimeZone(TimeZone.getDefault());
            df.setTimeZone(TimeZone.getTimeZone("UTC"));

            return df.format(today);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String toLocalTZ(String time) {
        try {
            TimeZone timeZone1 = TimeZone.getTimeZone("UTC");
            TimeZone timeZone2 = TimeZone.getDefault();
            SimpleDateFormat format = new SimpleDateFormat(this.fmt + " H:mm");
            SimpleDateFormat l = new SimpleDateFormat(this.fmt + " h:mm a");

            format.setTimeZone(timeZone1);
            Date d1 = format.parse(time);

            l.setTimeZone(timeZone2);
            String s = format.format(d1);

            d1 = format.parse(s);
            return l.format(d1);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String toUTCDate(String time, int formatType) {
        try {
            TimeZone timeZone1 = TimeZone.getTimeZone("UTC");
            TimeZone timeZone2 = TimeZone.getDefault();
            SimpleDateFormat format = new SimpleDateFormat(this.fmt + " H:mm:ss");
            SimpleDateFormat l;
            if (formatType == 0) {
                l = new SimpleDateFormat("yyyy/MM/dd");
            } else {
                l = new SimpleDateFormat(this.fmt2);
            }
            format.setTimeZone(timeZone1);
            Date d1 = format.parse(time);

            l.setTimeZone(timeZone2);
            String s = format.format(d1);

            d1 = format.parse(s);
            return l.format(d1);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean compareTime(String t1, String t2) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(this.fmt + " h:mm");
            Date d1 = format.parse(t1);
            Date d2 = format.parse(t2);

            return d1.after(d2);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static boolean isStreamReady(String t) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd h:mm a");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
            Date d1 = format.parse(getDate());
            Date d2 = format2.parse(getPSTTime1(t));
            Calendar cal = Calendar.getInstance();
            cal.setTime(d2);
            cal.add(12, -20);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(d1);
            long d = cal2.getTimeInMillis();
            long d3 = cal.getTimeInMillis();
            return d >= d3;
        } catch (ParseException ex) {
            System.err.println(ex);
        }
        return false;
    }

    public static String getDate() {
        Date today = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd h:mm a");

        return df.format(today);
    }

    public static String getPrevDate() {
        Date today = new Date();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(5, -1);
        today = cal.getTime();

        return df.format(today);
    }

    public String getPSTDate(String text) {
        String d = text.split("/")[2] + "-" + text.split("/")[0] + "-" + text.split("/")[1];
        SimpleDateFormat sdf = new SimpleDateFormat(this.fmt);
        sdf.setLenient(false);
        try {
            sdf.parse(d);
        } catch (ParseException e) {
            System.err.println(e);
            return null;
        }
        return d;
    }

    public boolean isToday(String date) {
        Date today = new Date();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        today = cal.getTime();

        String tdy = getPSTDate(df.format(today));

        return tdy.equals(date);
    }

    public long nextDay()
            throws ParseException {
        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat(this.fmt + " h:mm a");
        SimpleDateFormat format2 = new SimpleDateFormat(this.fmt);
        format2.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
        Date d1 = format.parse(format2.format(date) + " 3:00 AM");
        cal.setTime(d1);

        Date d = format.parse(format2.format(date) + ' ' + getPSTTime());
        cal2.setTime(d);
        if (!cal.after(cal2)) {
            cal.add(5, 1);
        }
        return cal.getTimeInMillis() - cal2.getTimeInMillis();
    }

    public String getPSTTime() {
        Date today = new Date();
        DateFormat df = new SimpleDateFormat("h:mm a");

        df.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));

        return df.format(today);
    }

    public static String getPSTTime1(String t) {
        TimeZone timeZone1 = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
        df.setTimeZone(timeZone1);
        Date d2 = null;
        try {
            d2 = df.parse(t);
        } catch (ParseException ex) {
            Logger.getLogger(Timezone.class.getName()).log(Level.SEVERE, null, ex);
        }
        df.setTimeZone(TimeZone.getDefault());

        return df.format(d2);
    }

    public static String getPSTDateTime() {
        Date today = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd h:mm a");

        df.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));

        return df.format(today);
    }
}
