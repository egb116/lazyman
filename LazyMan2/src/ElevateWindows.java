import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class ElevateWindows
{
  String batName = "elevate.vbs";
  
  public ElevateWindows()
  {
    if (checkForUac())
    {
      elevate();
      System.exit(0);
    }
  }
  
  private boolean checkForUac()
  {
    File dummyFile = new File("c:/aaa.txt");
    dummyFile.deleteOnExit();
    try
    {
      FileWriter fw = new FileWriter(dummyFile, true);
    }
    catch (IOException ex)
    {
      return true;
    }
    return false;
  }
  
  private void elevate()
  {
    File file = new File(System.getProperty("java.io.tmpdir") + "" + this.batName);
    file.deleteOnExit();
    
    createBatchFile(file);
    
    runBatchFile();
  }
  
  private String getJarLocation()
  {
    return "\"\""+getClass().getProtectionDomain().getCodeSource().getLocation().getPath().substring(1).replaceAll("%20", " ")+"\"\"";
  }
  
  private void runBatchFile()
  {
    Runtime runtime = Runtime.getRuntime();
        String[] cmd = new String[]{"cmd.exe", "/C",
            "cscript " + System.getProperty("java.io.tmpdir") + "/" + batName};
        try {
            Process proc = runtime.exec(cmd);
            ProcessProps.getOutput(proc, null);
            //proc.waitFor();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
  }
  
  private void createBatchFile(File file)
  {
    try {
            try (FileWriter fw = new FileWriter(file, true)) {
                fw.write(
                        "Set objShell = CreateObject(\"Shell.Application\") \n"
                                + "args = Right(\"java -jar " + getJarLocation() + " try2\", (Len(\"java -jar " + getJarLocation() + " try2\") - Len(\"java\"))) \n"
                                + "objShell.ShellExecute \"java\", args, \"\", \"runas\", 0");
            }
        } catch (IOException ex) {
            //ex.printStackTrace();
        }
  }
}
