
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StartPrograms {

    private final String os;

    public StartPrograms(String opsys) {
        os = opsys;
    }

    public ProcessBuilder StartJAR(String loc, String gid, String ha, String pw) {
        ProcessBuilder jar;

        if (os.toLowerCase().contains("win")) {
            jar = new ProcessBuilder("java", "-jar", loc, gid, ha);
        } else {
            String p = "echo \'" + pw + "\' | sudo -S";
            jar = new ProcessBuilder("/bin/sh", "-c", p + " java -jar " + loc + " " + gid + " " + ha);
        }

        return jar;
    }

    public void startLMMac() {
        PrintWriter writer = null;
        try {

            Path currentRelativePath = Paths.get("");
            File slm = new File(currentRelativePath.toAbsolutePath().toString() + "/Start LazyMan");
            if (!slm.exists()) {
                writer = new PrintWriter(slm);
                writer.println("#!/bin/bash\n");
                writer.println("cd \"" + currentRelativePath.toAbsolutePath().toString() + "\"");
                writer.println("java -jar LazyMan2.jar mac");
                writer.close();
                new ProcessBuilder("/bin/sh", "-c", "chmod +x \"" + currentRelativePath.toAbsolutePath().toString() + "/Start LazyMan\"").start();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StartPrograms.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(StartPrograms.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StartPrograms.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
    }

    public ProcessBuilder StartVLC(String loc, String arg) {
        ProcessBuilder vlc;

        if (os.toLowerCase().contains("win")) {
            String a1 = arg.split(" ")[0], a2;
            try {
                a2 = arg.split(" ")[1];
                a2 += " " + arg.split(" ")[2];
                a2 += " " + arg.split(" ")[3];
                a2 += " " + arg.split(" ")[4];
            } catch (ArrayIndexOutOfBoundsException u) {
                a2 = "";
            }
            vlc = new ProcessBuilder("\"" + loc + "\"", a1, a2);
        } else {
            vlc = new ProcessBuilder("/bin/bash", "-c", "\"" + loc + "\" " + arg);
        }

        return vlc;
    }

    public void editHost(String pw, String line) {
        String p = "echo \'" + pw + "\' | sudo -S ";
        try {
            ProcessProps.getOutput(new ProcessBuilder("/bin/sh", "-c", p + "-- sh -c \"echo \'" + line + "\' >> /etc/hosts\"").redirectErrorStream(true).start(), null);
        } catch (IOException ex) {

        }

    }

    public void setUpLS(String pw) {
        String p = "echo \'" + pw + "\' | sudo -S ";

        try {
            if (!cmdExists("pip")) {
                System.err.println("Pip not found. Installing...");
                ProcessProps.getOutput(new ProcessBuilder("/bin/sh", "-c", "curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py").redirectErrorStream(true).start(), null);
                ProcessProps.getOutput(new ProcessBuilder("/bin/sh", "-c", p + "python get-pip.py").redirectErrorStream(true).start(), null);
                ProcessProps.getOutput(new ProcessBuilder("/bin/sh", "-c", p + "rm get-pip.py").redirectErrorStream(true).start(), null);
            }
            if (!cmdExists("livestreamer")) {
                System.err.println("Livestreamer not found... Installing...");
                ProcessProps.getOutput(new ProcessBuilder("/bin/sh", "-c", p + "easy_install -U livestreamer").redirectErrorStream(true).start(), null);
            }

            ProcessProps.getOutput(new ProcessBuilder("/bin/sh", "-c", "pip install --user pycrypto").redirectErrorStream(true).start(), null);
        } catch (IOException ex) {

        }
    }

    public static void killJAR(String pw) {
        try {
            String buf, b = "";
            String p = "echo \'" + pw + "\' | sudo -S ";
            Process pb = new ProcessBuilder("/bin/sh", "-c", p + "lsof -i :80").start();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(pb.getInputStream()));
            int i = 0;
            while ((buf = stdInput.readLine()) != null) {
                if (i >= 1) {
                    if (buf.contains("java") && buf.contains("root")) {
                        b = buf;
                        break;
                    }
                }
                i++;
            }
            if (!b.equals("")) {
                int j = b.indexOf("java"), r = b.indexOf("root");

                String pid = b.substring(j + 4, r);
                pid = pid.replace("\t", "").replace(" ", "");
                System.out.println(pid);

                new ProcessBuilder("/bin/sh", "-c", p + "kill " + pid).start();
            }
        } catch (IOException ex) {

        }
    }

    public void kill80(String os, String pw) {

        String buf, b = "";
        if (!os.contains("win")) {
            try {
                String p = "echo \'" + pw + "\' | sudo -S ", pid = "";
                ArrayList<String> info = new ArrayList<>();
                Process pb = new ProcessBuilder("/bin/sh", "-c", p + "lsof -i :80 | grep LISTEN").start();
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(pb.getInputStream()));
                int i = 0, l = 0, pidConsec = 0;
                String c;
                while ((buf = stdInput.readLine()) != null) {
                    b = buf;
                    while (l < b.length()) {
                        c = Character.toString(b.charAt(l));
                        try {
                            i = Integer.parseInt(c);
                            pid += i;
                            pidConsec++;
                        } catch (NumberFormatException nfe) {
                            if (pidConsec > 0) {
                                pidConsec = 0;
                                break;
                            }
                        }
                        l++;
                    }
                    if (!pid.equals("")) {
                        info.add(pid);
                        pid = "";
                    }

                }
                if (info.size() > 0) {
                    pid = "";
                    for (i = 0; i < info.size(); i++) {
                        pid = info.get(i);
                        new ProcessBuilder("/bin/sh", "-c", p + "kill " + pid).start();
                    }
                }
            } catch (IOException ex) {

            }
        } else {
            try {
                ArrayList<String> info = new ArrayList<>();
                Process process = Runtime.getRuntime().exec("cmd /c netstat -a -n -o | findstr \":80\"");
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
                int l;
                while ((buf = stdInput.readLine()) != null) {
                    b = buf.replaceAll(" ", "");
                    l = b.indexOf("LISTENING") + 9;
                    if (l > 8) {
                        info.add(b.substring(l));
                    }

                }
                if (!b.equals("")) {
                    String pid = "";
                    for (int i = 0; i < info.size(); i++) {
                        pid = info.get(i);
                        Runtime.getRuntime().exec("cmd /c taskkill /F /PID " + pid);
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public ProcessBuilder StartLS(String[] arg) {
        return new ProcessBuilder(arg);
    }

    public void modifyHost(File hostsFile, String line) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader(hostsFile);
        String s;
        String totalStr = "";
        BufferedReader br;
        br = new BufferedReader(fr);

        while ((s = br.readLine()) != null) {
            if (s.contains(line.split(" ")[1])) {
                s = line;
            }
            totalStr += s + "\r\n";
        }
        br.close();
        FileWriter fw = new FileWriter(hostsFile);
        fw.write(totalStr);
        fw.close();
    }

    public void modifyHost(File hostsFile, String line, String pw) {
        String p = "echo \'" + pw + "\' | sudo -S ";
        if (os.toLowerCase().contains("mac")) {
            try {
                ProcessProps.getOutput(new ProcessBuilder("/bin/sh", "-c", p + "sed -E -i '' \"s/^ *[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+( +" + line.split(" ")[1] + ")/" + line.split(" ")[0] + "\\1/\" /etc/hosts").start(), null);
            } catch (IOException ex) {

            }
        } else {
            try {
                ProcessProps.getOutput(new ProcessBuilder("/bin/sh", "-c", p + "sed -r -i \"s/^ *[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+( +" + line.split(" ")[1] + ")/" + line.split(" ")[0] + "\\1/\" /etc/hosts").redirectErrorStream(true).start(), null);
            } catch (IOException ex) {

            }
        }
    }

    private boolean cmdExists(String cmd) {
        StringBuilder b = new StringBuilder();
        try {
            int i = 0;
            Process p = new ProcessBuilder("/bin/sh", "-c", cmd).redirectErrorStream(true).start();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

            while (((p.isAlive()) || (stdInput.ready())) && i < 2) {
                if (stdInput.ready()) {
                    try {
                        while (stdInput.ready() && i < 2) {
                            b.append(stdInput.readLine());
                            i++;
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return !(b.toString().toLowerCase().contains("not found") || b.toString().toLowerCase().contains("no file"));
    }
}
