
import java.awt.TextArea;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcessProps {

    public static void getOutput(Process p, TextArea ta) {
        try {
            String b = null;

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

            while ((p.isAlive()) || (stdInput.ready())) {
                if (stdInput.ready()) {
                    try {
                        while (stdInput.ready()) {
                            b = stdInput.readLine();
                            System.out.println(b);
                            if (ta != null) {
                                ta.append(b + "\n");
                            }
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                Thread.sleep(300);
            }
        } catch (IOException ex) {
             ex.printStackTrace();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}

