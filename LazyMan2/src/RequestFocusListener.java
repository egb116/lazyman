/*    */ import java.awt.Component;
/*    */ import java.awt.Window;
/*    */ import java.awt.event.HierarchyEvent;
/*    */ import java.awt.event.HierarchyListener;
/*    */ import java.awt.event.WindowAdapter;
/*    */ import java.awt.event.WindowEvent;
/*    */ import javax.swing.SwingUtilities;
/*    */ 
/*    */ public class RequestFocusListener
/*    */   implements HierarchyListener
/*    */ {
/*    */   public void hierarchyChanged(HierarchyEvent e)
/*    */   {
/* 14 */     final Component c = e.getComponent();
/* 15 */     if ((c.isShowing()) && ((e.getChangeFlags() & 0x4) != 0L)) {
/* 16 */       Window toplevel = SwingUtilities.getWindowAncestor(c);
/* 17 */       toplevel.addWindowFocusListener(new WindowAdapter()
/*    */       {
/*    */         public void windowGainedFocus(WindowEvent e)
/*    */         {
/* 21 */           c.requestFocus();
/*    */         }
/*    */       });
/*    */     }
/*    */   }
/*    */ }


/* Location:              D:\Program Files\NetBeansProjects\LazyMan2\LazyMan2.jar!\RequestFocusListener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */