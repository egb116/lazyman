
import java.awt.Desktop;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingWorker;

public class NHLStreams extends javax.swing.JFrame {

    private FindURL f;
    private String os, date, arg = "";
    private String pw;
    private Prop p;
    private boolean vlcdone, hasCmt = false, lsReady = false, lsDone = false, cantEditHost = false, setFavGmSettings;
    private StartPrograms sp;
    private final String ver = "2.1.0.20160416-02";

    private void checkHostFile() {
        String ip = "", line = "107.6.175.141 mf.svc.nhl.com";
        try {
            ip = Arrays.toString(InetAddress.getAllByName("mf.svc.nhl.com"));
        } catch (UnknownHostException ex) {
            ip = "n";
        }
        if (ip.contains("107.6.175.141")) {
            hostLbl.setText("Host file edited.");
        } else {
            java.io.File hostsFile;
            if (os.contains("mac")) {
                hostsFile = new java.io.File("/private/etc/hosts");
            } else if (os.contains("windows")) {
                hostsFile = new java.io.File(System.getenv("WINDIR")
                        + "\\System32\\drivers\\etc\\hosts");
            } else {
                hostsFile = new java.io.File("/etc/hosts");
            }

            boolean foundLine = false;

            try {
                try ( // Construct BufferedReader from InputStreamReader
                        java.io.FileInputStream fis = new java.io.FileInputStream(hostsFile);
                        java.io.BufferedReader br = new java.io.BufferedReader(
                                new java.io.InputStreamReader(fis))) {
                    String line1;
                    while ((line1 = br.readLine()) != null) {
                        if (line1.contains(line.split(" ")[1])) {
                            if (line1.contains(line.split(" ")[0])) {
                                foundLine = true;
                                br.close();
                                hasCmt = line1.contains("#");
                                break;
                            } else if (os.toLowerCase().contains("win")) {
                                try {
                                    sp.modifyHost(hostsFile, line);
                                    foundLine = true;
                                    br.close();
                                    break;
                                } catch (java.io.FileNotFoundException e) {
                                    new ElevateWindows();
                                } catch (java.io.IOException e) {

                                    e.printStackTrace();
                                }
                            } else {
                                sp.modifyHost(hostsFile, line, pw);
                                foundLine = true;
                                br.close();
                                break;
                            }
                        }
                    }
                }

                if (!foundLine) {
                    hostLbl.setText("Host file not edited.");
                } else if (hasCmt) {
                    hostLbl.setText("Host file edited but has #.");
                } else {
                    hostLbl.setText("Host file edited.");
                }
                if (!foundLine && os.toLowerCase().contains("win") && !arg.equals("try2")) {

                    try (java.io.PrintWriter o = new java.io.PrintWriter(new java.io.BufferedWriter(
                            new java.io.FileWriter(hostsFile, true)))) {
                        o.println("\n107.6.175.141 mf.svc.nhl.com");
                        o.close();
                        hostLbl.setText("Host file edited.");
                    } catch (java.io.FileNotFoundException e) {
                        cantEditHost = true;
                    } catch (java.io.IOException e) {

                        e.printStackTrace();
                    }
                } else if (arg.equals("try2") && !foundLine) {
                    try (java.io.PrintWriter o = new java.io.PrintWriter(new java.io.BufferedWriter(
                            new java.io.FileWriter(hostsFile, true)))) {
                        o.println("\n" + line);
                        o.close();
                        hostLbl.setText("Host file edited.");
                    } catch (java.io.FileNotFoundException e) {
                        String message = "Could not edit your host file. Please follow the guide on how to modify its permissions.";

                        int mc = JOptionPane.ERROR_MESSAGE;
                        JOptionPane.showMessageDialog(null, message, "Error", mc);
                    } catch (java.io.IOException e) {
                        int mc = JOptionPane.ERROR_MESSAGE;
                        JOptionPane.showMessageDialog(null, e, "Error", mc);
                        e.printStackTrace();
                    }
                }
                if (!foundLine && !os.toLowerCase().contains("win")) {

                    StartPrograms s = new StartPrograms(os);
                    s.editHost(pw, line);
                    hostLbl.setText("Host file edited.");

                }

            } catch (java.io.IOException e) {
                hostLbl.setText("Host file not edited.");
                e.printStackTrace();
            }
        }
    }

    /**
     * Creates new form NHLStreams
     *
     * @throws java.io.IOException
     */
    public NHLStreams(String args) throws java.io.IOException, InterruptedException {
        this.vlcdone = false;
        arg = args;
        os = System.getProperty("os.name").toLowerCase();

        //OS X needs to be ran from terminal for some reason for livestreamer to work properly 
        if (os.toLowerCase().contains("mac") && !args.contains("mac")) {
            sp = new StartPrograms(os);
            sp.startLMMac();
            String message = "Please open LazyMan with the 'Start LazyMan' file";

            int mc = JOptionPane.ERROR_MESSAGE;
            JOptionPane.showMessageDialog(null, message, "Error", mc);
            System.exit(0);
        }
        initComponents();
        checkForUpdates();
        if (os.toLowerCase().contains("win")) {
            changePWMI.setVisible(false);
        }
        this.setLocationRelativeTo(null);

        Thread gameInfo = new newDay();

        gameInfo.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        p = new Prop(os);
        sp = new StartPrograms(os);

        if (!os.toLowerCase().contains("win")) {
            getPW();
            sp.setUpLS(pw);
        } else {
            pw = "";
        }
        checkHostFile();
        if (os.contains("win") && cantEditHost) {
            new ElevateWindows();
        }
        updateVLCLabel();
        updateBitrate();
        updateCDN();

        SwingWorker<Void, Void> gg = getNHLGames();

        gg.execute();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        fileChooser = new javax.swing.JFileChooser();
        jMenuItem1 = new javax.swing.JMenuItem();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        buttonGroup7 = new javax.swing.ButtonGroup();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        setVLCBtn = new javax.swing.JButton();
        fnTA = new java.awt.TextArea();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        hostLbl = new javax.swing.JLabel();
        cdnl3nlRdBtn = new javax.swing.JRadioButton();
        level3RdBtn = new javax.swing.JRadioButton();
        vlcLbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        gameList = new javax.swing.JList();
        RdBtn360p = new javax.swing.JRadioButton();
        homeRdBtn = new javax.swing.JRadioButton();
        awayRdBtn = new javax.swing.JRadioButton();
        playBtn = new javax.swing.JButton();
        RdBtn720p = new javax.swing.JRadioButton();
        RdBtn540p = new javax.swing.JRadioButton();
        frRdBtn = new javax.swing.JRadioButton();
        RdBtn720p60 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        dateTF = new javax.swing.JFormattedTextField();
        chooseBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        updateBtn = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        optionsMI = new javax.swing.JMenuItem();
            changePWMI = new javax.swing.JMenuItem();
            closeMI = new javax.swing.JMenuItem();
            aboutM = new javax.swing.JMenu();

            jRadioButtonMenuItem1.setSelected(true);
            jRadioButtonMenuItem1.setText("jRadioButtonMenuItem1");

            jMenuItem1.setText("jMenuItem1");

            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
            setTitle("LazyMan 2");
            setFocusCycleRoot(false);
            setResizable(false);

            setVLCBtn.setText("Set");
            setVLCBtn.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    setVLCBtnMouseClicked(evt);
                }
            });

            jLabel3.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            jLabel3.setText("Console output:");

            jLabel2.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            jLabel2.setText("Set VLC/MPC Path:");

            hostLbl.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            hostLbl.setText("Checking Host file...");

            buttonGroup3.add(cdnl3nlRdBtn);
            cdnl3nlRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            cdnl3nlRdBtn.setText("Akamai");
            cdnl3nlRdBtn.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    cdnRBAP(evt);
                }
            });

            buttonGroup3.add(level3RdBtn);
            level3RdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            level3RdBtn.setSelected(true);
            level3RdBtn.setText("Level 3");
            level3RdBtn.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    cdnRBAP(evt);
                }
            });

            vlcLbl.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            vlcLbl.setText("jLabel4");

            gameList.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
            gameList.setModel(new javax.swing.AbstractListModel() {
                String[] strings = { "Loading..." };
                public int getSize() { return strings.length; }
                public Object getElementAt(int i) { return strings[i]; }
            });
            gameList.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    gameListMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(gameList);

            buttonGroup2.add(RdBtn360p);
            RdBtn360p.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            RdBtn360p.setText("360p");
            RdBtn360p.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    bitrateRdBtnActionPerformed(evt);
                }
            });

            buttonGroup1.add(homeRdBtn);
            homeRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            homeRdBtn.setText("Home");
            homeRdBtn.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    homeRdBtnActionPerformed(evt);
                }
            });

            buttonGroup1.add(awayRdBtn);
            awayRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            awayRdBtn.setSelected(true);
            awayRdBtn.setText("Away");

            playBtn.setText("Play");
            playBtn.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    playBtnMouseClicked(evt);
                }
            });

            buttonGroup2.add(RdBtn720p);
            RdBtn720p.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            RdBtn720p.setText("720p");
            RdBtn720p.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    bitrateRdBtnActionPerformed(evt);
                }
            });

            buttonGroup2.add(RdBtn540p);
            RdBtn540p.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            RdBtn540p.setSelected(true);
            RdBtn540p.setText("540p");
            RdBtn540p.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    bitrateRdBtnActionPerformed(evt);
                }
            });

            buttonGroup1.add(frRdBtn);
            frRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            frRdBtn.setText("French");

            buttonGroup2.add(RdBtn720p60);
            RdBtn720p60.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            RdBtn720p60.setText("720p 60fps");
            RdBtn720p60.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    bitrateRdBtnActionPerformed(evt);
                }
            });

            jLabel6.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
            jLabel6.setText("Date:");

            try {
                dateTF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
            } catch (java.text.ParseException ex) {
                ex.printStackTrace();
            }

            chooseBtn.setText("Choose");
            chooseBtn.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    chooseBtnMouseClicked(evt);
                }
            });

            jLabel1.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
            jLabel1.setText("Feed:");

            jLabel4.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
            jLabel4.setText("Quality:");

            jLabel5.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
            jLabel5.setText("CDN:");

            updateBtn.setText("New version available! Click here");
            updateBtn.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    updateBtnMouseClicked(evt);
                }
            });

            javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
            jPanel2.setLayout(jPanel2Layout);
            jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(18, 18, 18)
                            .addComponent(setVLCBtn))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(10, 10, 10)
                            .addComponent(vlcLbl)))
                    .addGap(19, 19, 19))
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(awayRdBtn)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(homeRdBtn)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(frRdBtn))
                                .addComponent(jLabel1)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGap(12, 12, 12)
                                    .addComponent(playBtn))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(RdBtn540p)
                                    .addGap(11, 11, 11)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(RdBtn720p)
                                        .addComponent(RdBtn720p60)))
                                .addComponent(RdBtn360p)
                                .addComponent(jLabel4)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(level3RdBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cdnl3nlRdBtn))
                                .addComponent(jLabel5))
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(fnTA, javax.swing.GroupLayout.PREFERRED_SIZE, 453, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel6)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(dateTF, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(chooseBtn)))
                            .addGap(0, 27, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                            .addComponent(updateBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(hostLbl)
                            .addGap(40, 40, 40))))
            );
            jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(dateTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chooseBtn))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(setVLCBtn)))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(vlcLbl)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(28, 28, 28)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(24, 24, 24)
                            .addComponent(jLabel1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(homeRdBtn)
                                .addComponent(awayRdBtn)
                                .addComponent(frRdBtn))
                            .addGap(36, 36, 36)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(RdBtn360p)
                                .addComponent(RdBtn720p, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(RdBtn720p60)
                                .addComponent(RdBtn540p))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(level3RdBtn)
                                .addComponent(cdnl3nlRdBtn))
                            .addGap(26, 26, 26)
                            .addComponent(playBtn)
                            .addGap(32, 32, 32)))
                    .addComponent(fnTA, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(hostLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(updateBtn)))
            );

            jTabbedPane2.addTab("NHL", jPanel2);

            jMenu1.setText("File");

            optionsMI.setText("Options");
            optionsMI.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    optionsMIMouseClicked(evt);
                }
            });
            optionsMI.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    optionsMIActionPerformed(evt);
                }
            });
            jMenu1.add(optionsMI);

            changePWMI.setText("Change Password");
            changePWMI.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    changePWMIActionPerformed(evt);
                }
            });
            jMenu1.add(changePWMI);
       

        closeMI.setText("Close");
        closeMI.setFocusCycleRoot(true);
        closeMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeMIActionPerformed(evt);
            }
        });
        jMenu1.add(closeMI);

        jMenuBar1.add(jMenu1);

        aboutM.setText("About");
        aboutM.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                aboutMMouseClicked(evt);
            }
        });
        jMenuBar1.add(aboutM);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleDescription("");

        setBounds(0, 0, 511, 648);
    }// </editor-fold>//GEN-END:initComponents

    private void optionsMIMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_optionsMIMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_optionsMIMouseClicked

    private void optionsMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_optionsMIActionPerformed
        // TODO add your handling code here:
        Options op = new Options(this, true);
        op.setLocationRelativeTo(this);
        op.setVisible(true);
        if (!p.getNHLTeam().equals("None")) {
            int index;
            String in = f.getFavTeamIndex(p.getNHLTeam());

            index = Integer.parseInt(in.substring(0, in.length() - 1));
            if (Timezone.isStreamReady(f.GetDate(gameList.getSelectedIndex()) + " " + f.GetTime(gameList.getSelectedIndex()))) {
                gameList.setSelectedIndex(index);
                gameList.ensureIndexIsVisible(gameList.getSelectedIndex());

                if (in.charAt(in.length() - 1) == 'h' || !f.isAvailable("AWAY", index)) {
                    homeRdBtn.setSelected(true);
                } else {
                    awayRdBtn.setSelected(true);
                }
            } else {
                gameList.setSelectedIndex(0);
            }
        }
    }//GEN-LAST:event_optionsMIActionPerformed

    private void gameListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gameListMouseClicked
        // TODO add your handling code here:
        getAvailableStreams();
        if (!awayRdBtn.isEnabled()) {
            homeRdBtn.setSelected(true);
        }
    }//GEN-LAST:event_gameListMouseClicked

    private void updateBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateBtnMouseClicked
        try {
            // TODO add your handling code here:
            URL url = new URL("https://www.reddit.com/r/LazyMan/wiki/downloads");
            URI uri = url.toURI();
            Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                try {
                    desktop.browse(uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_updateBtnMouseClicked

    private void closeMIActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        System.out.println("Exiting...");
        close();
    }// GEN-LAST:event_jMenuItem2ActionPerformed

    private void aboutMMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenu2MouseClicked
        int mc = JOptionPane.INFORMATION_MESSAGE;
        JOptionPane.showMessageDialog(null, "Ver. " + ver + " \nMade by /u/StevensNJD4", "About", mc);
    }// GEN-LAST:event_jMenu2MouseClicked

    private void chooseBtnMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_chooseBtnMouseClicked
        // TODO add your handling code here:
        String d = dateTF.getText();
        if (!d.equals("  /  /    ")) {
            date = d;
            try {
                int i;
                FindURL f2 = new FindURL(getDate(d), 0);
                i = f2.GamesFound();
                f = f2;

                DefaultListModel model = new DefaultListModel();
                if (i == 1) {
                    ArrayList games = f.getTdyGms();

                    for (Object game : games) {
                        model.addElement(game);
                    }
                    gameList.clearSelection();
                    gameList.setModel(model);

                    if (p.getNHLTeam().equals("") || p.getNHLTeam().equals("None")) {
                        gameList.setSelectedIndex(0);
                        gameList.repaint();
                    } else {
                        String in = f.getFavTeamIndex(p.getNHLTeam());
                        int index = Integer.parseInt(in.substring(0, in.length() - 1));
                        if (index != -1) {
                            Timezone tz = new Timezone();
                            if (f.GetGameState(index).equals("In Progress") || !tz.isToday(date)) {
                                gameList.setSelectedIndex(index);
                                gameList.ensureIndexIsVisible(gameList.getSelectedIndex());

                                gameList.repaint();
                                setFavGmSettings = true;
                                if (in.charAt(in.length() - 1) == 'h' || !f.isAvailable("AWAY", index)) {
                                    homeRdBtn.setSelected(true);
                                }
                            } else {
                                gameList.setSelectedIndex(0);
                                gameList.repaint();
                            }
                        } else {
                            gameList.setSelectedIndex(0);
                            gameList.repaint();
                        }
                    }
                    playBtn.setEnabled(true);

                    getAvailableStreams();

                } else if (i == 2) {
                    model.addElement("Error getting games!");
                    model.addElement("Maybe we can't go back that far?");
                    gameList.setModel(model);
                    playBtn.setEnabled(false);
                } else if (i == -2) {
                    model.addElement("Error getting games!");
                    model.addElement("You are offline.");
                    gameList.setModel(model);
                    playBtn.setEnabled(false);
                } else {
                    model.addElement("No games today!");
                    gameList.setModel(model);
                    playBtn.setEnabled(false);
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                int mc = JOptionPane.ERROR_MESSAGE;
                JOptionPane.showMessageDialog(null, "Please enter a date in MM/DD/YYYY format.", "Error", mc);
            }
        } else {
            int mc = JOptionPane.ERROR_MESSAGE;
            JOptionPane.showMessageDialog(null, "Please enter a date in MM/DD/YYYY format.", "Error", mc);
        }
    }// GEN-LAST:event_chooseBtnMouseClicked

    private void setVLCBtn1MouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_setVLCBtn1MouseClicked
        // TODO add your handling code here:
        setVLCBtnMouseClicked(evt);
    }// GEN-LAST:event_setVLCBtn1MouseClicked

    private void bitrateRdBtnActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_bitrateRdBtnActionPerformed
        String bitrate = "";
        if (RdBtn360p.isSelected()) {
            bitrate = "360p";
        } else if (RdBtn540p.isSelected()) {
            bitrate = "540p";
        } else if (RdBtn720p.isSelected()) {
            bitrate = "720p";
        } else if (RdBtn720p60.isSelected()) {
            bitrate = "best";
        }
        p.setBitrate(bitrate);
    }// GEN-LAST:event_bitrateRdBtnActionPerformed

    private void playBtnMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_playBtnMouseClicked
        // TODO add your handling code here:
        checkHostFile();
        SwingWorker<Void, Void> pl = playLive();
        pl.execute();
    }// GEN-LAST:event_playBtnMouseClicked

    private void homeRdBtnActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_homeRdBtnActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_homeRdBtnActionPerformed

    private void cdnRBAP(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cdnRBAP
        // TODO add your handling code here:
        String cdn = "";

        if (level3RdBtn.isSelected()) {
            cdn = "Level 3";
        } else if (cdnl3nlRdBtn.isSelected()) {
            cdn = "Akamai";
        }

        p.setCDN(cdn);
    }// GEN-LAST:event_cdnRBAP

    private void setVLCBtnMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_setVLCBtnMouseClicked
        // TODO add your handling code here:
        String loc;

        fileChooser.setFileFilter(null);

        int returnVal = fileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            loc = fileChooser.getSelectedFile().getPath();

            if (loc.toLowerCase().contains("vlc")
                    || loc.toLowerCase().contains("mpc")) {
                p.setVlcloc(loc);
                vlcLbl.setText("Location set.");;
            } else {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane
                        .showConfirmDialog(
                                null,
                                "This doesn't look like a VLC or MPC-HC path. Set anyway?",
                                "", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    p.setVlcloc(loc);
                    vlcLbl.setText("Location set.");
                } else {
                    vlcLbl.setText("Location not set.");
                }
            }
        }
    }// GEN-LAST:event_setVLCBtnMouseClicked

    private void changePWMIActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_changePWMIActionPerformed
        // TODO add your handling code here:
        if (p.getPW().equals("")) {
            getPW();
        } else {
            String p1 = null, p2;
            while (true) {
                JPasswordField pf = new JPasswordField();
                pf.addHierarchyListener(new RequestFocusListener());
                JPanel pPanel = new JPanel();
                pPanel.setLayout(new BoxLayout(pPanel, BoxLayout.Y_AXIS));
                pPanel.add(new JLabel("Enter your password"));
                pPanel.add(pf);
                JPasswordField pf2 = new JPasswordField();
                pf2.addHierarchyListener(new RequestFocusListener());
                JPanel pPanel2 = new JPanel();
                pPanel2.setLayout(new BoxLayout(pPanel2, BoxLayout.Y_AXIS));
                pPanel2.add(new JLabel("Enter your password again"));
                pPanel2.add(pf2);
                int okCxl = JOptionPane
                        .showConfirmDialog(null, pPanel, "Sudo",
                                JOptionPane.OK_CANCEL_OPTION,
                                JOptionPane.PLAIN_MESSAGE);

                if (okCxl == JOptionPane.OK_OPTION) {
                    p1 = new String(pf.getPassword());
                } else {
                    break;
                }

                int okCxl2 = JOptionPane.showConfirmDialog(null, pPanel2,
                        "Sudo", JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.PLAIN_MESSAGE);

                if (okCxl2 == JOptionPane.OK_OPTION) {
                    p2 = new String(pf2.getPassword());
                } else {
                    continue;
                }

                if (!p1.isEmpty() && (p1 == null ? p2 == null : p1.equals(p2))) {
                    pw = p1;
                    try {
                        p.setPW(Encryption.encrypt(pw));
                        JOptionPane
                                .showMessageDialog(null, "Password changed!");
                    } catch (Exception ex) {

                    }

                    break;
                }

                JOptionPane.showMessageDialog(null,
                        "Passwords don't match. Try again.");
            }
        }
    }// GEN-LAST:event_changePWMIActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws InterruptedException {
        try {
            /*
             * java.awt.EventQueue.invokeLater(new Runnable() {
             * 
             * @Override public void run() { try { new
             * NHLStreams().setVisible(true); } catch (GeneralSecurityException
             * ex) {
             * 
             * } catch (UnsupportedEncodingException ex) {
             * 
             * } catch (java.io.IOException ex) {
             * 
             * }
             * 
             * } });
             */
            try {
                new NHLStreams(args[0]).setVisible(true);
            } catch (ArrayIndexOutOfBoundsException e) {
                new NHLStreams("").setVisible(true);
            }

            /* Create and display the form */
        } catch (java.io.IOException ex) {

        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton RdBtn360p;
    private javax.swing.JRadioButton RdBtn540p;
    private javax.swing.JRadioButton RdBtn720p;
    private javax.swing.JRadioButton RdBtn720p60;
    private javax.swing.JMenu aboutM;
    private javax.swing.JRadioButton awayRdBtn;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.ButtonGroup buttonGroup7;
    private javax.swing.JRadioButton cdnl3nlRdBtn;
    private javax.swing.JMenuItem changePWMI;
    private javax.swing.JButton chooseBtn;
    private javax.swing.JMenuItem closeMI;
    private javax.swing.JFormattedTextField dateTF;
    private javax.swing.JFileChooser fileChooser;
    private java.awt.TextArea fnTA;
    private javax.swing.JRadioButton frRdBtn;
    private javax.swing.JList gameList;
    private javax.swing.JRadioButton homeRdBtn;
    private javax.swing.JLabel hostLbl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private final javax.swing.JMenu jMenu1 = new javax.swing.JMenu();
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JRadioButton level3RdBtn;
    private javax.swing.JMenuItem optionsMI;
    private javax.swing.JButton playBtn;
    private javax.swing.JButton setVLCBtn;
    private javax.swing.JButton updateBtn;
    private javax.swing.JLabel vlcLbl;
    // End of variables declaration//GEN-END:variables

    private String getVLCLoc() {
        String loc;
        java.io.File file;
        loc = p.getVlcloc();

        if (loc.equals("")) {
            if (os.toLowerCase().contains("win")) {
                loc = "C:\\Program Files\\VideoLAN\\VLC\\vlc.exe";
                file = new java.io.File(loc);
                if (file.exists()) {
                    p.setVlcloc(loc);
                    return loc;
                } else {
                    loc = "C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe";
                    file = new java.io.File(loc);
                    if (file.exists()) {
                        p.setVlcloc(loc);
                        return loc;
                    }
                }
            } else if (os.toLowerCase().contains("mac")) {
                loc = "/Applications/VLC.app/Contents/MacOS/VLC";
                file = new java.io.File(loc);
                if (file.exists()) {
                    p.setVlcloc(loc);
                    return loc;
                }
            } else {
                loc = "/usr/bin/vlc";
                file = new java.io.File(loc);
                if (file.exists()) {
                    p.setVlcloc(loc);
                    return loc;
                }
            }
        } else {
            return loc;
        }
        return "";
    }

    private String buildURL(String away, String home, int id) {
        Timezone tz = new Timezone();
        String feedType = "";
        if (awayRdBtn.isSelected()) {
            feedType = "VISIT";
        } else if (homeRdBtn.isSelected()) {
            if (f.isAvailable("HOME", id)) {
                feedType = "HOME";
            } else {
                feedType = "NATIONAL";
            }
        } else {
            feedType = "FRENCH";
        }
        String cdn = null;

        /*if ((gc1RB.isSelected() || gcam2RB.isSelected())
                && Integer.parseInt(bitrate) > 1600) {
            bitrate = "1600";
        }*/
        if (level3RdBtn.isSelected()) {
            cdn = "l3c";
        } else if (cdnl3nlRdBtn.isSelected()) {
            cdn = "akc";
        }
        String duplicate = "http://hlslive-" + cdn + ".med2.med.nhl.com/ls04/nhl/" + f.GetDate(gameList.getSelectedIndex()).replaceAll("-", "/") + "/NHL_GAME_VIDEO_" + away + home + "_M2_" + feedType + "_" + f.GetDate(gameList.getSelectedIndex()).replaceAll("-", "") + "_1/master_wired60.m3u8";
        String url = "http://hlslive-" + cdn + ".med2.med.nhl.com/ls04/nhl/" + f.GetDate(gameList.getSelectedIndex()).replaceAll("-", "/") + "/NHL_GAME_VIDEO_" + away + home + "_M2_" + feedType + "_" + f.GetDate(gameList.getSelectedIndex()).replaceAll("-", "") + "/master_wired60.m3u8";
        boolean working = f.testM3U8(duplicate);
        if (tz.isToday(date)) {
            if (working) {
                return duplicate;
            }
            return url;
        } else {
            String rurl = "http://hlsvod-akc.med2.med.nhl.com/ps01/nhl/" + f.GetDate(gameList.getSelectedIndex()).replaceAll("-", "/") + "/NHL_GAME_VIDEO_" + away + home + "_M2_" + feedType + "_" + f.GetDate(gameList.getSelectedIndex()).replaceAll("-", "") + "/master_wired60.m3u8";
            String rurldup = "http://hlsvod-akc.med2.med.nhl.com/ps01/nhl/" + f.GetDate(gameList.getSelectedIndex()).replaceAll("-", "/") + "/NHL_GAME_VIDEO_" + away + home + "_M2_" + feedType + "_" + f.GetDate(gameList.getSelectedIndex()).replaceAll("-", "") + "_1/master_wired60.m3u8";
            working = f.testM3U8(rurldup);

            if (working) {
                return rurldup;
            } else {
                working = f.testM3U8(rurl);
                if (working) {
                    return rurl;
                }
                return url;
            }
        }
    }

    private String[] argsToStr(String away, String home, int id) {
        String bitrate = null;

        if (RdBtn360p.isSelected()) {
            bitrate = "360p";
        } else if (RdBtn540p.isSelected()) {
            bitrate = "540p";
        } else if (RdBtn720p.isSelected()) {
            bitrate = "720p";
        } else if (RdBtn720p60.isSelected()) {
            bitrate = "best";
        }

        String ls, nk = "";
        if (bitrate.equals("best")) {
            nk = " name_key=bitrate";
        }
        if (os.toLowerCase().contains("win")) {
            Path currentRelativePath = Paths.get("");
            ls = currentRelativePath.toAbsolutePath().toString() + "\\livestreamer-v1.12.2\\livestreamer.exe";
            return new String[]{ls, "--player", "\"" + getVLCLoc() + "\"", "\"hlsvariant://" + buildURL(away, home, id) + nk + "\"", bitrate, "--http-no-ssl-verify"};
        } else {
            ls = "livestreamer";
            return new String[]{ls, "--player", "\"" + getVLCLoc() + "\"", "hlsvariant://" + buildURL(away, home, id) + nk, bitrate, "--http-no-ssl-verify"};
        }
    }

    private SwingWorker<Void, Void> startVLC(final ProcessBuilder vlc) {

        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                System.out.println("vlc starting...");
                Process v = vlc.start();
                vlcdone = false;
                v.waitFor();
                vlcdone = true;
                System.out.println("vlc done");
                return null;

            }

        };
        return worker;
    }

    private SwingWorker<Void, Void> startLiveStreamer(final ProcessBuilder ls) {

        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                fnTA.append("livestreamer starting...\n");
                ls.redirectErrorStream(true);

                lsDone = false;
                Process l = ls.start();
                try {
                    ProcessProps.getOutput(l, fnTA);
                } catch (Exception e) {
                    fnTA.append(e.getMessage());
                }

                vlcdone = false;
                while (l.isAlive()) {
                    Thread.sleep(700);
                }
                vlcdone = true;
                fnTA.append("livestreamer done");
                System.out.println("livestreamer done");
                lsReady = false;
                lsDone = true;
                return null;

            }

        };
        return worker;
    }

    private void updateVLCLabel() {
        if (!p.getVlcloc().equals("") || !getVLCLoc().equals("")) {
            vlcLbl.setText("Location already set.");
        } else {
            vlcLbl.setText("Location not set.");
        }
    }

    private String extendAbbr(String team) {
        switch (team) {
            case "OTT":
                return "senators";
            case "TOR":
                return "mapleleafs";
            case "MTL":
                return "canadiens";
            case "NJD":
                return "devils";
            case "ANA":
                return "ducks";
            case "ARI":
                return "coyotes";
            case "BOS":
                return "bruins";
            case "BUF":
                return "sabres";
            case "CAR":
                return "hurricanes";
            case "CBJ":
                return "bluejackets";
            case "CGY":
                return "flames";
            case "CHI":
                return "blackhawks";
            case "COL":
                return "avalanche";
            case "DAL":
                return "stars";
            case "DET":
                return "redwings";
            case "EDM":
                return "oilers";
            case "FLA":
                return "panthers";
            case "LAK":
                return "kings";
            case "MIN":
                return "wild";
            case "NYI":
                return "islanders";
            case "NYR":
                return "rangers";
            case "NSH":
                return "predators";
            case "PHI":
                return "flyers";
            case "PIT":
                return "penguins";
            case "SJS":
                return "sharks";
            case "STL":
                return "blues";
            case "TBL":
                return "lightning";
            case "VAN":
                return "canucks";
            case "WSH":
                return "capitals";
            case "WPG":
                return "jets";

            default:
                return "";
        }
    }

    private String getDate(String text) {
        Timezone tz = new Timezone();
        return tz.getPSTDate(text);
    }

    private void updateBitrate() {
        String bitrate = p.getBitrate();
        if (bitrate.equals("")) {
        } else if (bitrate.equals("360p")) {
            RdBtn360p.setSelected(true);
        } else if (bitrate.equals("540p")) {
            RdBtn540p.setSelected(true);
        } else if (bitrate.equals("720p")) {
            RdBtn720p.setSelected(true);
        } else if (bitrate.equals("best")) {
            RdBtn720p60.setSelected(true);
        }
    }

    private void updateCDN() {
        String cdn = p.getCDN();

        switch (cdn) {
            case "Level 3":
                level3RdBtn.setSelected(true);
                break;
            case "Akamai":
                cdnl3nlRdBtn.setSelected(true);
                break;
            default:
                return;
        }
    }

    private void getPW() {
        if (p.getPW().equals("")) {
            String p1 = null, p2;
            while (true) {
                JPasswordField pf = new JPasswordField();
                pf.addHierarchyListener(new RequestFocusListener());
                JPanel pPanel = new JPanel();
                pPanel.setLayout(new BoxLayout(pPanel, BoxLayout.Y_AXIS));
                pPanel.add(new JLabel("Enter your password"));
                pPanel.add(pf);
                JPasswordField pf2 = new JPasswordField();
                pf2.addHierarchyListener(new RequestFocusListener());
                JPanel pPanel2 = new JPanel();
                pPanel2.setLayout(new BoxLayout(pPanel2, BoxLayout.Y_AXIS));
                pPanel2.add(new JLabel("Enter your password again"));
                pPanel2.add(pf2);
                int okCxl = JOptionPane
                        .showConfirmDialog(null, pPanel, "Sudo",
                                JOptionPane.OK_CANCEL_OPTION,
                                JOptionPane.PLAIN_MESSAGE);

                if (okCxl == JOptionPane.OK_OPTION) {
                    p1 = new String(pf.getPassword());
                } else {
                    System.exit(0);
                }

                int okCxl2 = JOptionPane.showConfirmDialog(null, pPanel2,
                        "Sudo", JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.PLAIN_MESSAGE);

                if (okCxl2 == JOptionPane.OK_OPTION) {
                    p2 = new String(pf2.getPassword());
                } else {
                    continue;
                }

                if (!p1.isEmpty() && (p1 == null ? p2 == null : p1.equals(p2))) {
                    pw = p1;
                    int dialogButton = JOptionPane.YES_NO_OPTION;
                    int dialogResult = JOptionPane.showConfirmDialog(null,
                            "Would you like me to save your password?",
                            "Save Password", dialogButton);
                    if (dialogResult == JOptionPane.YES_OPTION) {
                        try {
                            p.setPW(Encryption.encrypt(pw));
                        } catch (Exception ex) {

                        }
                    }
                    break;
                }

                JOptionPane.showMessageDialog(null,
                        "Passwords don't match. Try again.");
            }
        } else {
            try {
                pw = Encryption.decrypt(p.getPW());
            } catch (Exception ex) {

            }
        }
    }

    private String getPSTDate() {
        Timezone tz = new Timezone();
        return tz.getPSTDate(0);
    }

    public void close() {
        System.exit(0);
    }

    private SwingWorker<Void, Void> openLmMac() {

        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                sp.startLMMac();
                return null;

            }

        };
        return worker;
    }

    private SwingWorker<Void, Void> playLive() {

        SwingWorker<Void, Void> worker;
        worker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                if (!getVLCLoc().equals("")) {
                    int index1 = gameList.getSelectedIndex();
                    if (Timezone.isStreamReady(f.GetDate(index1) + f.GetTime(index1))) {
                        lsDone = false;
                        fnTA.setText("");
                        ProcessBuilder ls = sp.StartLS(argsToStr(f.GetAwayTeam(index1), f.GetHomeTeam(index1), index1));
                        SwingWorker<Void, Void> worker2 = startLiveStreamer(ls);
                        worker2.execute();

                        while (!lsReady && !lsDone) {
                            lsReady = fnTA.getText().contains("Opening");
                            Thread.sleep(700);
                        }
                        if (!lsReady) {
                            String message = "Stream unavailable";

                            int mc = JOptionPane.ERROR_MESSAGE;
                            JOptionPane.showMessageDialog(null, message, "Error", mc);
                        }
                    } else {
                    String message = "Please wait until 20 minutes before game time.";

                    int mc = JOptionPane.ERROR_MESSAGE;
                    JOptionPane.showMessageDialog(null, message, "Error", mc);
                }
                } else {
                    String message = "Please set the location to the VLC or MPC executable.";

                    int mc = JOptionPane.ERROR_MESSAGE;
                    JOptionPane.showMessageDialog(null, message, "Error", mc);
                }
                return null;

            }

        };
        return worker;
    }

    private void getAvailableStreams() {
        int index = gameList.getSelectedIndex();
        if (!f.isAvailable("AWAY", index)) {
            awayRdBtn.setEnabled(false);
            homeRdBtn.setSelected(true);
        } else {
            awayRdBtn.setEnabled(true);
        }

        if (f.isAvailable("NATIONAL", index)) {
            homeRdBtn.setText("National");
        } else {
            homeRdBtn.setText("Home");
        }

        if (!f.isAvailable("FRENCH", index)) {
            frRdBtn.setEnabled(false);
        } else {
            frRdBtn.setEnabled(true);
        }
    }

    class newDay extends Thread {

        public void run() {
            Timezone tz = new Timezone();
            while (true) {
                f = new FindURL(tz.getPSTDate(0), 0);
                date = getPSTDate();
                dateTF.setText(tz.getPSTDate(2));
                setFavGmSettings = false;
                try {
                    Thread.sleep(tz.nextDay());
                    SwingWorker<Void, Void> gg = getNHLGames();
                    gg.execute();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    private SwingWorker<Void, Void> getNHLGames() {

        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                while (true) {

                    try {
                        int i;
                        i = f.GamesFound();

                        DefaultListModel model = new DefaultListModel();
                        if (i == 1) {
                            ArrayList games = f.getTdyGms();

                            for (Object game : games) {
                                model.addElement(game);
                            }

                            gameList.setModel(model);
                            gameList.repaint();
                            if (p.getNHLTeam().equals("") || p.getNHLTeam().equals("None")) {
                                gameList.setSelectedIndex(0);
                                gameList.repaint();
                            } else {
                                String in = f.getFavTeamIndex(p.getNHLTeam());
                                int index = Integer.parseInt(in.substring(0, in.length() - 1));
                                if (index != -1) {
                                    if (f.GetGameState(index).equals("In Progress")) {
                                        gameList.setSelectedIndex(index);
                                        gameList.ensureIndexIsVisible(gameList.getSelectedIndex());

                                        gameList.repaint();
                                        setFavGmSettings = true;
                                        if (in.charAt(in.length() - 1) == 'h' || !f.isAvailable("AWAY", index)) {
                                            homeRdBtn.setSelected(true);
                                        }
                                    } else {
                                        gameList.setSelectedIndex(0);
                                        gameList.repaint();
                                    }
                                } else {
                                    gameList.setSelectedIndex(0);
                                    gameList.repaint();
                                }
                            }

                            getAvailableStreams();
                            break;
                        } else if (i == 2) {
                            model.addElement("Error getting games!");
                            model.addElement("Try restarting LazyMan 2.");
                            gameList.setModel(model);
                            playBtn.setEnabled(false);

                            Thread.sleep(20000);
                        } else if (i == -2) {
                            model.addElement("Error getting games!");
                            model.addElement("You are offline.");
                            gameList.setModel(model);
                            playBtn.setEnabled(false);

                            break;
                        } else {
                            model.addElement("No games today!");
                            gameList.setModel(model);
                            playBtn.setEnabled(false);

                            break;
                        }

                        System.gc();

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                return null;

            }
        };
        return worker;
    }

    private void checkForUpdates() {
        String currentVer = "";
        Reader r = null;

        URLConnection con;
        URL website = null;
        try {
            try {
                website = new URL("https://bitbucket.org/ntyler92/lazyman/raw/master/LazyMan2/VERSION");
            } catch (MalformedURLException j) {
                currentVer = "down";
            }
            con = website.openConnection();
            InputStream is = con.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            char[] charArray = new char['?'];
            StringBuilder sb = new StringBuilder();
            int numCharsRead;
            while ((numCharsRead = isr.read(charArray)) > 0) {
                sb.append(charArray, 0, numCharsRead);
            }
            currentVer = sb.toString();
        } catch (IOException ex) {
            System.err.println(ex);
            currentVer = "";
        } catch (NullPointerException n) {
            n.printStackTrace();
            currentVer = "offline";
        } finally {
            try {
                if (r != null) {
                    r.close();
                    r = null;
                }
            } catch (IOException localIOException5) {
            }
        }
        if (currentVer.equals("down") || currentVer.equals("") || currentVer.equals("offline") || currentVer.equals(ver)) {
            updateBtn.setVisible(false);
        }
    }
}
